package com.bugra.weatherapi;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("weather")
public class Controller {

    @Autowired
    CityName cityName;

    @Value("${esma}")
    String str;

    RestTemplate restTemplate = new RestTemplate();

    String url = "http://api.weatherapi.com/v1/current.json?key=3a285091fce743c0a05100233200110&q=%s";

    @GetMapping("istanbul")
    public ResponseEntity<WeatherDto> foo() {
        String citiesName = cityName.getName().get(0);
        String urlEnd = String.format(url, citiesName);
        ResponseEntity<WeatherDto> forEntity = restTemplate.getForEntity(urlEnd, WeatherDto.class);
        return forEntity;
    }

    @GetMapping("izmir")
    public ResponseEntity<WeatherDto> bar() {
        String citiesName = cityName.getName().get(1);
        String urlEnd = String.format(url, citiesName);
        ResponseEntity<WeatherDto> forEntity = restTemplate.getForEntity(urlEnd, WeatherDto.class);
        return forEntity;
    }


    @GetMapping("ankara")
    public ResponseEntity<WeatherDto> tar() {
        String citiesName = cityName.getName().get(2);
        String urlEnd = String.format(url, citiesName);
        ResponseEntity<WeatherDto> forEntity = restTemplate.getForEntity(urlEnd, WeatherDto.class);
        return forEntity;
    }

    @GetMapping()
    public void kal() {
        //properties java düzeyi bugra -> tugberk
        String bugra = System.getProperty("bugra");
        System.out.println(bugra);
        //environments işletim sistemi düzeyi esma -> ertan
        String esma = System.getenv("esma");
        System.out.println(esma);
        System.out.println(str);
    }



}



